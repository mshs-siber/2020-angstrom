#!/usr/bin/env python3

from flask import Flask, request
app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello World!"

if __name__ == "__main__":
    cert="/etc/letsencrypt/live/c.science2do.com/fullchain1.pem"
    key="/etc/letsencrypt/live/c.science2do.com/privkey1.pem"
    app.run(host="0.0.0.0", port=443, ssl_context=(cert,key))
